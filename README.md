# 最新spring boot项目地址
[![Fork me on Gitee](https://gitee.com/wangziyangyang/MES-Springboot/widgets/widget_6.svg?color=1a17e6)](https://gitee.com/wangziyangyang/MES-Springboot)
# 大家好,本人从事MES开发,此框架越用越难用。。。。（2020年03月04日停止更新） 


# 我重新开一套框架（springBoot）项目。 分布式项目 完成MES

## 借此框架，想改造一款，性能更高的架构。目前还处在交流分享阶段。后续会根据大家实际需求优化代码。
## 我先介绍下如何起动该项目！
               （我给这套MES系统起了一个名字叫做"领头羊"）
[![wangziyangyang/MES](https://gitee.com/wangziyangyang/MES/widgets/widget_card.svg?colors=ffffff,1e252b,323d47,455059,d7deea,99a0ae)](https://gitee.com/wangziyangyang/MES)

## 【技术架构】
### SSM+shiro+mysql+ehcache+mp(mybatis-plus)
完美引入mp帮助开发者快速生产所需要的资源
AutoGenerator 是 MyBatis-Plus 的代码生成器，通过 AutoGenerator 可以快速生成 Entity、Mapper、Mapper XML、Service、Controller 等各个模块的代码，极大的提升了开发效率。

引入hutool工具类。官服文档https://hutool.cn/docs/#/ 

### 成功展示
1）.登录验证界面
![登录界面](https://s2.ax1x.com/2020/02/29/3sCWDI.png)
2）.主菜单界面
![主菜单](https://s2.ax1x.com/2020/02/29/3scZi4.png)
3）.功能预览界面
![功能界面](https://s2.ax1x.com/2020/02/29/3scnzR.png)

## 安装教程

 ### 1.首先安装Mysql数据库
具体的步骤请各自百度，以及连接工具navicat的安装
打开navicat(自行破解就行)新建数据库命名iemes_v1即可

执行数据库脚本，目录：src\main\webapp\dbscript\iemes_v1.sql等待创建数据
这里面我看过有一些历史记录的数据。。可以自行删除一些插入数据
![功能界面](https://s2.ax1x.com/2020/02/29/3sge78.png)
### 2.使用IDEA 克隆https://gitee.com/wangziyangyang/MES.git 
检索代码后配置MAVEN pom.xml文件 有一个mysql驱动版本要和你装的数据库版本对应我这里安装的是mysql8 这是最新版。。所以用的是 8.0.12版本


        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.12</version>
        </dependency>
        
        
        
### 3.把所有的jar包准备完毕后 maven引入tomcat插件 
项目里面也是有jetty 插件，看个人喜好都行
 ```
 <plugin>
    <groupId>org.apache.tomcat.maven</groupId>
  <artifactId>tomcat7-maven-plugin</artifactId>
 <version>2.2</version>
   <configuration>
   <port>8080</port>
<path>/</path>
<uriEncoding>utf-8</uriEncoding>
 <ignorePackaging>true</ignorePackaging>
 </configuration>
</plugin>
 ```
 ### 4.修改jdbc.properties该文件
 ![](https://s2.ax1x.com/2020/02/29/3s2rGQ.png)
### 5.配置maven服务启动项目即可
![](https://s2.ax1x.com/2020/02/29/3sRdyR.png)

## 关键代码分析
MyRealm.Java 这个类是shiro验证权限用的。

用户权限表ly_user
![](https://s2.ax1x.com/2020/02/29/3sWkc9.png)
重写了doGetAuthorizationInfo方法 和doGetAuthenticationInfo
```
String username = (String) token.getPrincipal();
UserFormMap userFormMap = new UserFormMap();
userFormMap.put("accountName", "" + username + "");
List<UserFormMap> userFormMaps = userMapper.findByNames(userFormMap);
```

BackgroundController.Java 是登录的控制器
```
// 想要得到 SecurityUtils.getSubject()　的对象．．访问地址必须跟ｓｈｉｒｏ的拦截地址内．不然后会报空指针
Subject user = SecurityUtils.getSubject();
// 用户输入的账号和密码,,存到UsernamePasswordToken对象中..然后由shiro内部认证对比,
// 认证执行者交由ShiroDbRealm中doGetAuthenticationInfo处理
// 当以上认证成功后会向下执行,认证失败会抛出异常
UsernamePasswordToken token = new UsernamePasswordToken(username, password);

```

mp 需要执行CodeGenerator.Java main方法

成功示例.
执行后需要输入，某个一目录存放生成资源，以及自己的表名称
![](https://s2.ax1x.com/2020/03/03/3f2tiR.png)

## 总结
群Q87652870 为个人建立的MES开发交流群。
欢迎各位加入，为实际需求做贡献